import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  final String judul, message;

  AboutPage({Key key, this.judul, this.message}) : super(key: key);

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.judul}'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Pesan Anda: ${widget.message}'),
            FlatButton(
                onPressed: () => Navigator.pop(context), child: Text('Home'))
          ],
        ),
      ),
    );
  }
}
