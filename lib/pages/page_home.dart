import 'package:flutter/material.dart';
import 'package:latihan_navigasi/pages/page_about.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController _messageController;

  @override
  void initState() {
    _messageController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
              padding: EdgeInsets.all(18.0),
              child: TextField(
                controller: _messageController,
              )),
          SizedBox(
            height: 20,
          ),
          FlatButton(
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AboutPage(
                                  judul: 'Judul Screen',
                                  message: _messageController.text,
                                )))
                  },
              child: Text('About page')),
        ],
      )),
    );
  }
}
